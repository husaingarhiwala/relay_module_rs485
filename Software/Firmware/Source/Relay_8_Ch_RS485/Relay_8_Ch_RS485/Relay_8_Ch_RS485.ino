#include <SimpleModbusSlave.h>

#include <EEPROM.h>

/* 
   SimpleModbusSlaveV10 supports function 3, 6 & 16.
   
   This example code implements the Modbus RTU functionality for controlling 
   8 Relays connected through a ULN 2803 Driver.
   The relays can be controlled by using holdingRegs[0] to holdingRegs[7]
   
   The modbus_update() method updates the holdingRegs register array and checks
   communication.
   
  Digital Output
 
  8 Relays are connected through ULN2003 relay driver on the Digital pins
  These can be set by using this code


  Relay Connections
  R1 = A1
  R2 = A0
  R3 = D10
  R4 = D9
  R5 = D8
  R6 = D7
  R7 = D6
  R8 = D5

  
*/

#define R1 A1
#define R2 A0
#define R3 10
#define R4 9
#define R5 8
#define R6 7
#define R7 6
#define R8 5

// Using the enum instruction allows for an easy method for adding and 
// removing registers. Doing it this way saves you #defining the size 
// of your slaves register array each time you want to add more registers
// and at a glimpse informs you of your slaves register layout.

//////////////// registers of your slave ///////////////////
enum 
{     
  // just add or remove registers and your good to go...
  // The first register starts at address 0
  RELAY1,
  RELAY2,
  RELAY3,
  RELAY4,
  RELAY5,
  RELAY6,
  RELAY7,
  RELAY8,
  SLAVE_ID,
  BAUD_RATE,
        
  HOLDING_REGS_SIZE // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};

int addr=0;
long baud=0;  
unsigned int holdingRegs[HOLDING_REGS_SIZE]; // function 3 and 16 register array

void setup()
{
      int baud_LSB=0,baud_MSB=0;

  /* parameters(HardwareSerial* SerialPort,
                long baudrate, 
    unsigned char byteFormat,
                unsigned char ID, 
                unsigned char transmit enable pin, 
                unsigned int holding registers size,
                unsigned int* holding register array)
  */
  
  /* Valid modbus byte formats are:
     SERIAL_8N2: 1 start bit, 8 data bits, 2 stop bits
     SERIAL_8E1: 1 start bit, 8 data bits, 1 Even parity bit, 1 stop bit
     SERIAL_8O1: 1 start bit, 8 data bits, 1 Odd parity bit, 1 stop bit
     
     These byte formats are already defined in the Arduino global name space. 
  */
  
  modbus_configure(&Serial, 9600, SERIAL_8N1, 1, 8, HOLDING_REGS_SIZE, holdingRegs);

  // modbus_update_comms(baud, byteFormat, id) is not needed but allows for easy update of the
  // port variables and slave id dynamically in any function.
  

  pinMode(R1, OUTPUT);
  pinMode(R2, OUTPUT);
  pinMode(R3, OUTPUT);
  pinMode(R4, OUTPUT);
  pinMode(R5, OUTPUT);
  pinMode(R6, OUTPUT);
  pinMode(R7, OUTPUT);
  pinMode(R8, OUTPUT);

  addr = EEPROM.read(0);

    baud_LSB = EEPROM.read(2);
    baud_MSB = EEPROM.read(4);
    baud = baud_LSB + (baud_MSB*255);
    
  if((baud == 4800) || (baud == 9600) ||  (baud == 19200)|| (baud == 38400))
  {  
  }
  else
  {
      baud = 9600;
  }
  
  if((addr < 1) || (addr > 32)) 
        addr=1;       

  modbus_update_comms(baud, SERIAL_8N1, addr);
  
  holdingRegs[BAUD_RATE] = baud;
  holdingRegs[SLAVE_ID] = addr;
  
}

void loop()
{
  volatile int temp=0,temp2=0;
  
  modbus_update();
  
  if(holdingRegs[RELAY1]==1)      digitalWrite(R1, HIGH); 
  else                            digitalWrite(R1, LOW);

  if(holdingRegs[RELAY2]==1)      digitalWrite(R2, HIGH); 
  else                            digitalWrite(R2, LOW);

  if(holdingRegs[RELAY3]==1)      digitalWrite(R3, HIGH); 
  else                            digitalWrite(R3, LOW);

   if(holdingRegs[RELAY4]==1)      digitalWrite(R4, HIGH); 
   else                            digitalWrite(R4, LOW);

  if(holdingRegs[RELAY5]==1)      digitalWrite(R5, HIGH); 
  else                            digitalWrite(R5, LOW);

  if(holdingRegs[RELAY6]==1)      digitalWrite(R6, HIGH); 
  else                            digitalWrite(R6, LOW);

  if(holdingRegs[RELAY7]==1)      digitalWrite(R7, HIGH); 
  else                            digitalWrite(R7, LOW);

  if(holdingRegs[RELAY8]==1)      digitalWrite(R8, HIGH); 
  else                            digitalWrite(R8, LOW);

   if(holdingRegs[BAUD_RATE] != baud)
   {
          temp = holdingRegs[BAUD_RATE];
          temp = temp/255;  
          
          EEPROM.write(4,temp);               //Write MSB
          temp = temp*255;
          temp2 = holdingRegs[BAUD_RATE] - temp;
          EEPROM.write(2,temp2);               //Write LSB  
          baud = holdingRegs[BAUD_RATE];

   }
        
   if(holdingRegs[SLAVE_ID] !=  addr)
   {
          EEPROM.write(0,holdingRegs[SLAVE_ID]); 
          addr = holdingRegs[SLAVE_ID];
   }


  
}

